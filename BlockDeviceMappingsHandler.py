#!/usr/bin/python3

class BlockDeviceMappingsHandler:
	def BuildBlockDeviceMappings(self, configurationYaml):
		blockDeviceMappings = [];
		for volume in configurationYaml["volumes"]:
			blockDeviceMapping = self.__BuildBlockDeviceMapping(volume);
			blockDeviceMappings.append(blockDeviceMapping);
		return blockDeviceMappings;
		
	def __BuildBlockDeviceMapping(self, volume):
		blockDeviceMapping = {};
		blockDeviceMapping["DeviceName"] = volume["device"];
		ebs = {};
		ebs["VolumeSize"] = volume["size_gb"];
		blockDeviceMapping["Ebs"] = ebs;
		return blockDeviceMapping;
		
	def __AddUser(self, user, userDataCommand):
		userLogin = user['login'];
		userSshKey = user['ssh_key'];
		sshDirectory = "/home/%s/.ssh" % userLogin;

		userDataCommand += "adduser %s" % userLogin;
		self.__AddNewLine(userDataCommand);

		userDataCommand += "mkdir -p %s" % sshDirectory;
		self.__AddNewLine(userDataCommand);

		userDataCommand += "touch %s/authorized_keys" % sshDirectory;
		self.__AddNewLine(userDataCommand);

		userDataCommand += "echo %s > %s/authorized_keys" % (userSshKey, sshDirectory);
		self.__AddNewLine(userDataCommand);

		userDataCommand += "chown -R %s:%s %s" % (userLogin, userLogin, sshDirectory);
		self.__AddNewLine(userDataCommand);

		userDataCommand += "chmod 600 %s/authorized_keys" % sshDirectory;
		self.__AddNewLine(userDataCommand);
		
	def __AddNewLine(self, userDataCommand):
		userDataCommand += "\n";
		
