#!/usr/bin/python3

import boto3
import sys

from BlockDeviceMappingsHandler import BlockDeviceMappingsHandler
from ImageHandler import ImageHandler
from InstanceHandler import InstanceHandler
from LoadConfiguration import LoadConfiguration
from PrintSshInformation import PrintSshInformation
from SecurityGroupIngressHandler import SecurityGroupIngressHandler
from UserDataHandler import UserDataHandler

class DeployInstance:
	def __init__(self, profileName, region):
		try:
			session = boto3.session.Session(profile_name = profileName);
		except Exception as ex:
			text = str(ex);
			if text == ("The config profile (%s) could not be found" % profileName):
				print("User profile name must be in ~/.aws/credentials");
			raise ex;
		self.__ValidateRegion(region, session);

		ec2Client = session.client("ec2", region_name = region);
		ec2Resource = session.resource("ec2", region_name = region);
		self.__ImageHandler = ImageHandler(ec2Client);
		self.__UserDataHandler = UserDataHandler();
		self.__InstanceHandler = InstanceHandler(ec2Resource, ec2Client);
		self.__BlockDeviceMappingsHandler = BlockDeviceMappingsHandler();
		self.__SecurityGroupIngressHandler = SecurityGroupIngressHandler(ec2Client);
		self.__PrintSshInformation = PrintSshInformation();
		
	def Run(self):
		loadConfiguration = LoadConfiguration();
		server = loadConfiguration.Server;
		image = self.__ImageHandler.Find(server);
		userDataCommand = self.__UserDataHandler.BuildUserDataCommand(server);
		blockDeviceMappings = self.__BlockDeviceMappingsHandler.BuildBlockDeviceMappings(server);
		instances = self.__InstanceHandler.Build(server, image, userDataCommand, blockDeviceMappings);
		for instance in instances:
			self.__SecurityGroupIngressHandler.OpenSshPort(instance);
			publicDnsName = instance["PublicDnsName"]
			for user in server["users"]:
				self.__PrintSshInformation.PrintForUser(publicDnsName, user);
		
	def __ValidateRegion(self, region, session):
		regions = session.get_available_regions("ec2");
		if region not in regions:
			print("Region %s not in available region list %s" % (region, regions));
			raise Exception();
	

if len(sys.argv) < 3:
	print("Please pass in a 'user profile name' from your ~/.aws/credentials as the first parameter and an 'aws ec2 region' as the second parameter.");
profileName = sys.argv[1];
region = sys.argv[2];
deployInstance = DeployInstance(profileName, region);
deployInstance.Run();
