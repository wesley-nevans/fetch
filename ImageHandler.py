#!/usr/bin/python3

class ImageHandler:
	def __init__(self, ec2Client):
		self.__EC2Client = ec2Client;
		
	def Find(self, configurationYaml):
		rootDevice = configurationYaml["volumes"][0];
		filters = [
			{
				"Name" : "architecture",
				"Values" : [configurationYaml["architecture"]]
			},
			{
				"Name" : "root-device-type",
				"Values" : [configurationYaml["root_device_type"]]
			},
			{
				"Name" : "virtualization-type",
				"Values" : [configurationYaml["virtualization_type"]]
			},
			{
				"Name" : "root-device-name",
				"Values" : [rootDevice["device"]]
			},
			{
				"Name" : "owner-alias",
				"Values" : ["amazon"]
			}
		];
		imagesResponse = self.__EC2Client.describe_images(Filters = filters, IncludeDeprecated = False);
		images = imagesResponse["Images"];
		filteredList = [];
		startName = "%s-ami-%s-" % (configurationYaml["ami_type"], configurationYaml["virtualization_type"]);
		endName = "-%s-%s" % (configurationYaml["architecture"], configurationYaml["root_device_type"]);
		for image in images:
			imageName = image["Name"];
			if imageName.startswith(startName) and imageName.endswith(endName):
				filteredList.append(image);
		filteredList.sort(key=lambda image: image["CreationDate"], reverse=True);
		return filteredList[0];
		
	def __FindRootDevice(self, configurationYaml):
		for volume in configurationYaml["volumes"]:
			if volume["mount"] == "/":
				return volume;
		return None;
