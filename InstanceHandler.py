#!/usr/bin/python3

class InstanceHandler:
	def __init__(self, ec2Resource, ec2Client):
		self.__EC2Resource = ec2Resource;
		self.__EC2Client = ec2Client;
		
	def Build(self, configurationYaml, image, userDataCommand, blockDeviceMappings):
		createdInstancesList = self.__EC2Resource.create_instances(
			ImageId = image["ImageId"],
			InstanceType = configurationYaml["instance_type"],
			MinCount = configurationYaml["max_count"],
			MaxCount = configurationYaml["min_count"],
			UserData = userDataCommand,
			BlockDeviceMappings = blockDeviceMappings
		);
		returnList = [];
		for createdInstance in createdInstancesList:
			instanceId = createdInstance.id;
			waiter = self.__EC2Resource.meta.client.get_waiter("instance_running");
			waiter.wait(InstanceIds = [instanceId]);
			reservationsResponse = self.__EC2Client.describe_instances(InstanceIds = [instanceId]);
			reservations = reservationsResponse["Reservations"];
			for reservation in reservations:
				instances = reservation["Instances"];
				for instance in instances:
					returnList.append(instance);
		return returnList;
		
