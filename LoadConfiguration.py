#!/usr/bin/python3

import os
import yaml

class LoadConfiguration:
	def __init__(self):
		currentFilePath = os.path.abspath(__file__);
		currentDirectory = os.path.dirname(currentFilePath);
		pathToYaml = os.path.join(currentDirectory, "configuration.yml");
		with open(pathToYaml, 'r') as yamlFile:
			fullYaml = yaml.safe_load(yamlFile);
		self.Server = fullYaml["server"];
		
