#!/usr/bin/python3

import os

class PrintSshInformation:
	def __init__(self):
		currentFilePath = os.path.abspath(__file__);
		self.__CurrentDirectory = os.path.dirname(currentFilePath);
		
	def PrintForUser(self, publicDnsName, user):
		userLogin = user['login'];
		pathToPrivateKey = os.path.join(self.__CurrentDirectory, userLogin);
		sshCommand = "ssh -i %s %s@%s" % (pathToPrivateKey, userLogin, publicDnsName);
		print("To ssh into %s with user %s, use the following command: %s" % (publicDnsName, userLogin, sshCommand));
		
