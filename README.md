This is a submission for the Fetch application exercise https://fetch-hiring.s3.amazonaws.com/devops-engineer/vm-deployment-aws.html

We will assume you are running Ubuntu 20.04 with sudo privileges.

# Setup
Before the first run, we need to do some setup.

Run the following commands in the command prompt in the directory this README.md file is located:
- `sudo apt install python3 pip3`
- `pip3 install -r Requirements.txt`
- `chmod +x DeployInstance.py`

Set up an aws account following https://portal.aws.amazon.com/billing/signup#/start

Set up a user following https://aws.amazon.com/premiumsupport/knowledge-center/create-access-key/

Add the user's credentials to the ~/.aws/credentials following https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html#shared-credentials-file


# Run
To run this program, run the following command in the command prompt in the directory this README.md file is located:

./DeployInstance.py YOUR-USER-NAME YOUR-REGION



For example, if your username in your ~/.aws/credentials is test-user and your region is us-east-2, the command would be:

`./DeployInstance.py test-user us-east-2`

# Final comments
I could not find a way to set the root partition to be ext4. From what I can tell, the root partition must be EBS if the root device type is ebs.
