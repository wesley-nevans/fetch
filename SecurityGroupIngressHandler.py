#!/usr/bin/python3

class SecurityGroupIngressHandler:
	def __init__(self, ec2Client):
		self.__EC2Client = ec2Client;
		self.__IpPermission = {
			"IpProtocol": "tcp",
			"FromPort": 22,
			"ToPort": 22,
			"IpRanges": [
				{
					"CidrIp": "0.0.0.0/0"
				}
			]
		};
		
	def OpenSshPort(self, instance):
		groupIds = [];
		for securityGroup in instance["SecurityGroups"]:
			groupId = securityGroup["GroupId"];
			groupIds.append(groupId);
		response = self.__EC2Client.describe_security_groups(GroupIds=groupIds);
		for securityGroup in response["SecurityGroups"]:
			groupId = securityGroup["GroupId"];
			ipPermissions = securityGroup["IpPermissions"];
			if not self.__AlreadyHasPort22Open(ipPermissions):
				self.__EC2Client.authorize_security_group_ingress(GroupId=groupId, IpPermissions = [self.__IpPermission]);
			
	def __AlreadyHasPort22Open(self, ipPermissions):
		for ipPermission in ipPermissions:
			try:
				if ipPermission["IpProtocol"] == self.__IpPermission["IpProtocol"] and ipPermission["FromPort"] == self.__IpPermission["FromPort"] and ipPermission["ToPort"] == self.__IpPermission["ToPort"] and ipPermission["IpRanges"] == self.__IpPermission["IpRanges"]:
					return True;
			except Exception:
				pass;
		return False;
		
