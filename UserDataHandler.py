#!/usr/bin/python3

class UserDataHandler:
	def BuildUserDataCommand(self, configurationYaml):
		userDataCommand = "#!/bin/bash";
		userDataCommand += self.__NewLine();
		userDataCommand += self.__NewLine();

		for volume in configurationYaml["volumes"]:
			userDataCommand += self.__AddVolume(volume);
		userDataCommand += self.__NewLine();

		for user in configurationYaml["users"]:
			userDataCommand += self.__AddUser(user);
		userDataCommand += self.__NewLine();

		return userDataCommand;
		
	def __AddVolume(self, volume):
		
		mount = volume['mount'];
		if mount == "/":
			return "";
		type = volume["type"];
		device = volume["device"];

		userDataCommand = "";

		userDataCommand += "mkfs.%s %s" % (type, device);
		userDataCommand += self.__NewLine();

		userDataCommand += "mkdir -p %s" % mount;
		userDataCommand += self.__NewLine();

		userDataCommand += "mount -o rw %s %s" % (device, mount);
		userDataCommand += self.__NewLine();

		userDataCommand += "chmod -R 777 %s" % mount;
		userDataCommand += self.__NewLine();
		
		userDataCommand += self.__NewLine();

		return userDataCommand;
		
	def __AddUser(self, user):
		userLogin = user['login'];
		userSshKey = user['ssh_key'];
		sshDirectory = "/home/%s/.ssh" % userLogin;

		userDataCommand = "";
		
		userDataCommand += "adduser %s" % userLogin;
		userDataCommand += self.__NewLine();

		userDataCommand += "mkdir -p %s" % sshDirectory;
		userDataCommand += self.__NewLine();

		userDataCommand += "touch %s/authorized_keys" % sshDirectory;
		userDataCommand += self.__NewLine();

		userDataCommand += "echo %s > %s/authorized_keys" % (userSshKey, sshDirectory);
		userDataCommand += self.__NewLine();

		userDataCommand += "chown -R %s:%s %s" % (userLogin, userLogin, sshDirectory);
		userDataCommand += self.__NewLine();

		userDataCommand += "chmod 600 %s/authorized_keys" % sshDirectory;
		userDataCommand += self.__NewLine();
		
		userDataCommand += self.__NewLine();
		
		return userDataCommand;
		
	def __NewLine(self):
		return "\n";
		
